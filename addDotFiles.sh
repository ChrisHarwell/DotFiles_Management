#/bin/bash


# dotfiles=($(for file in $(find ~/ -name ".*" -type f -maxdepth 1); do basename $file; done))

dotFilesHandler() {
    IFS=$'\n' dotfiles=($(for file in $(find ~/ -name ".*" -type f -maxdepth 1); do basename "$file"; done))
    for file in "${dotfiles[@]}"; do
        echo "$file"
    done
}

dotDirsHandler() {
    IFS=$'\n' dotdirs=($(for dir in $(find ~/ -name ".*" -type d -maxdepth 1); do basename "$dir"; done))
    for dir in "${dotdirs[@]}"; do
        # echo "$dir"
        chezmoi add "~/$dir"
    done
}

main() {
    if [ "$1" == "files" ]; then
        dotFilesHandler
    elif [ "$1" == "dirs" ]; then
        dotDirsHandler
    else
        echo "Please specify either 'files' or 'dirs' as an argument."
    fi
}

main "$@"
